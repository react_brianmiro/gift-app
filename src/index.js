import React from 'react';
import ReactDOM from 'react-dom';
import { GifeExpertApp } from './GifExpertApp';
import './index.css';

import reportWebVitals from './reportWebVitals';

ReactDOM.render(
    <GifeExpertApp />,
  document.getElementById('root')
);


reportWebVitals();
