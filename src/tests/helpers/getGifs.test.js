import { getGifs } from "../../helpers/getGifs";

describe('Testing getGifs Fetch', () => {
   
    test('should  get 10 elements', async () => {
       
        const gifs = await getGifs('One Puch');

        expect( gifs.length ).toBe( 10 );

    });
    
    test('should  check that array is empty', async () => {
       
        const gifs = await getGifs('');

        expect( gifs.length ).toBe( 0 );

    });
    
});
