import { shallow } from 'enzyme';
import { GifeExpertApp } from "../GifExpertApp";


describe('Testing in <GifExpertApp />', () => {
   
    test('should show the component GifExpertApp', () => {
       
        const wrapper = shallow( <GifeExpertApp /> );
        expect( wrapper ).toMatchSnapshot();
        
    });
    
    test('should show list of categories', () => {
        
        const categories = ['One Punch','Goku'];
        const wrapper = shallow( <GifeExpertApp defaultCategories={ categories } /> );
        
        expect( wrapper ).toMatchSnapshot();
        expect( wrapper.find('GifGrid').length).toBe( categories.length );
        
    });
    
});
