import { shallow } from 'enzyme';
import { GifGrid } from '../../components/GifGrid';
import { useFetchGifs } from '../../hooks/useFetchGifs';
jest.mock('../../hooks/useFetchGifs');

describe('Testing in component <GifGrid />', () => {
   
    const category = 'One Puch';

    test('should  to  show the component', () => {

        useFetchGifs.mockReturnValue({
            data :  [],
            loading : true
        });
        const wrapper = shallow( <GifGrid  category={ category } /> );

        expect( wrapper ).toMatchSnapshot();
    });
    
    test('should to show items when the images are upload useFetchGifs', () => {
       
        const gifs = [{
            id : 'ABC',
            url : 'https://localhost/cualquier/cosa.jpg',
            title : 'Cualquier cosa'
        }];

        useFetchGifs.mockReturnValue({
            data :  gifs,
            loading : false
        });

        const wrapper = shallow( <GifGrid  category={ category } /> );

        expect( wrapper ).toMatchSnapshot();

    });
    
});
