import { shallow } from "enzyme";
import { GifGridItem } from "../../components/GifGridItem";


describe('Testing on  <GifGridItem />', () => {
    
    const title = 'Un titulo';
    const  url = 'https://localhost/img.jpg';
    const wrapper = shallow( <GifGridItem  title={ title } url={ url } />);
    
    test('show component success ', () => {       

        expect( wrapper ).toMatchSnapshot();

    });
    
    test('should have a <p> with the title prop', () => {
        
        const p = wrapper.find('p');

        expect( p.text().trim() ).toBe( title );

    });

    test('should have a <img> with src and alt', () => {
        
        const img = wrapper.find('img');

        expect( img.props().src ).toBe( url );
        expect( img.props().alt ).toBe( title );

    });
    
    test('should  have className animate__backInRight ', () => {
    
        const div = wrapper.find('div');
        const className = div.prop('className');

        expect( className.includes('animate__backInRight') ).toBe( true );

    });

    
});
