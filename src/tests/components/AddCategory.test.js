import { AddCategory } from '../../components/AddCategory';
import { shallow } from 'enzyme';

describe('Testing in <AddCategory />', () => {

    const setCategories =jest.fn();
    let  wrapper = shallow( <AddCategory  setCategories= { setCategories } /> );
   
    beforeEach( () => {
        jest.clearAllMocks();
        wrapper = shallow( <AddCategory  setCategories= { setCategories } /> );
    });

    test('should show the component ', () => {
        
        expect( wrapper ).toMatchSnapshot();
    });
    
    test('should chenge input', () => {
       
        const input = wrapper.find('input');
        const value = 'Hola Mundo';
        input.simulate('change', { target : { value } } );

    });

    test('not send data when OnSubmit is call', () => {
       
        wrapper.find('form').simulate('submit', { preventDefault(){}} );

        expect( setCategories ).not.toHaveBeenCalled();
    });
    
    test('should call setCategories and clear the input', () => {

        const value = 'Hola Mundo';
        
        const input = wrapper.find('input');
      
        input.simulate('change', { target : { value } } );

        wrapper.find('form').simulate('submit', { preventDefault(){}} );

        expect( setCategories ).toHaveBeenCalled();

        expect( input.prop('value')).toBe('');

    });
    
});
 